let eyeIcon = document.getElementsByTagName("i")[0];
let eyeIconSecond = document.getElementsByTagName("i")[1];
let inputOne = document.forms[0].elements[0];
let inputSecond = document.forms[0].elements[1];
let button = document.querySelector(".btn");
let text = null;
console.log(text);
function showPass() {
  if (inputOne.type === "password") {
    inputOne.type = "text";
    eyeIcon.classList.remove("fa-eye");
    eyeIcon.classList.add("fa-eye-slash");
  } else {
    inputOne.type = "password";
    eyeIcon.classList.remove("fa-eye-slash");
    eyeIcon.classList.add("fa-eye");
  }
}
function ShowPassSecond() {
  if (inputSecond.type === "password") {
    inputSecond.type = "text";
    eyeIconSecond.classList.remove("fa-eye");
    eyeIconSecond.classList.add("fa-eye-slash");
  } else {
    inputSecond.type = "password";
    eyeIconSecond.classList.remove("fa-eye-slash");
    eyeIconSecond.classList.add("fa-eye");
  }
}

function passwordValidation(e) {
  e.preventDefault();
  if (text !== null) {
    text.remove();
  }
  if (inputOne.value == "" && inputSecond.value == "") {
    createErrorMessage("Потрібно ввести значення");
  } else if (inputOne.value === inputSecond.value) {
    alert("You are welcome");
  } else {
    createErrorMessage("Потрібно ввести однакові значення");
  }
}

function createErrorMessage(message) {
  text = document.createElement("p");
  text.textContent = message;
  text.style.color = "red";
  inputSecond.after(text);
}

eyeIcon.addEventListener("click", showPass);
eyeIconSecond.addEventListener("click", ShowPassSecond);
button.addEventListener("click", passwordValidation);
